create table book (
    book_id int primary key auto_increment,
    book_title varchar(100),
    publisher_name varchar(100),
    author_name varchar(100));

insert into book (book_title,publisher_name,author_name) values("abc","Madhura","Sham");    
insert into book (book_title,publisher_name,author_name) values("xys","Shlok","Ram");